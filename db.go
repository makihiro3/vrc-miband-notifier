package main

import (
	"bytes"
	"encoding/gob"
	"errors"
	"time"

	"github.com/rs/xid"
	bolt "go.etcd.io/bbolt"
)

type Record struct {
	Time  int64
	Score float64
}

func NewRecordFromBytes(b []byte) (Record, error) {
	buf := bytes.NewBuffer(b)
	dec := gob.NewDecoder(buf)
	var r Record
	err := dec.Decode(&r)
	return r, err
}

func (r Record) Bytes() ([]byte, error) {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(r)
	return buf.Bytes(), err
}

const (
	Alpha = 1.0
	Beta  = 2.0
)

var Bucket = []byte("hoge")
var ErrorBucketNotFound = errors.New("Bucket not found")

var db *bolt.DB

func Open(path string) error {
	var err error
	db, err = bolt.Open(path, 0666, nil)
	if err != nil {
		return err
	}

	err = db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(Bucket)
		return err
	})
	if err != nil {
		db.Close()
		return err
	}
	return nil
}

func update(id xid.ID) error {
	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(Bucket)
		if b == nil {
			return ErrorBucketNotFound
		}

		ib := id.Bytes()
		val := b.Get(ib)
		var r Record
		if val == nil {
			r.Time = time.Now().UnixNano()
			r.Score = 0
		} else {
			var err error
			r, err = NewRecordFromBytes(val)
			if err != nil {
				return err
			}
			t := time.Now().UnixNano()
			d := t - r.Time
			r.Score += Alpha*float64(d) + Beta
			if r.Score < 0 {
				r.Score = 0.0
			}
			r.Time = t
		}
		bs, err := r.Bytes()
		if err != nil {
			return err
		}
		return b.Put(ib, bs)
	})
}

func view() (map[xid.ID]Record, error) {
	d := map[xid.ID]Record{}
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(Bucket)
		if b == nil {
			return ErrorBucketNotFound
		}
		c := b.Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {
			buf := bytes.NewBuffer(v)
			dec := gob.NewDecoder(buf)
			var r Record
			err := dec.Decode(&r)
			if err != nil {
				return err
			}
			id, err := xid.FromBytes(k)
			if err != nil {
				return err
			}
			d[id] = r
		}
		return nil
	})
	return d, err
}
