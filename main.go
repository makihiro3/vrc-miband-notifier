package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/rs/xid"
)

func id(w http.ResponseWriter, _ *http.Request) {
	id := xid.New()
	err := update(id)
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	io.WriteString(w, id.String())
}

func list(w http.ResponseWriter, _ *http.Request) {
	d, err := view()
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	for k, v := range d {
		fmt.Fprintf(w, "ID:%s Time:%s Score:%f\n", k, time.Unix(0, v.Time), v.Score)
	}
}

func main() {
	const path = "db.bolt"
	var err error
	err = Open(path)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("db open: %s", path)
	defer func() {
		err := db.Close()
		if err != nil {
			log.Fatal(err)
		}
		log.Print("db closed")
	}()

	http.HandleFunc("/id", id)
	http.HandleFunc("/list", list)

	srv := http.Server{Addr: ":8080"}
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		err = srv.ListenAndServe()
		if err != nil {
			log.Print(err)
		}
	}()
	<-c
	if err := srv.Shutdown(context.Background()); err != nil {
		log.Printf("HTTP server Shutdown: %v", err)
	}
}
